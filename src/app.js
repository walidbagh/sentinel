import 'bootstrap';
import Vue from 'vue';
import App from './App.vue';
import $ from 'jquery/dist/jquery.slim';
import swal from 'sweetalert2/src/sweetalert2';
import axios from 'axios';

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timerProgressBar: true,
    timer: 3000,
    onOpen: (t) => {
        t.addEventListener('mouseenter', swal.stopTimer);
        t.addEventListener('mouseleave', swal.resumeTimer);
    },
});

window.$ = window.jQuery = $;

new Vue({
    render: h => h(App)
}).$mount('#app');

export {
    $, axios, swal, toast,
};